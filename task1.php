<?php

function reverseString($str) {
    // Функция должна выводить последовательность символов в обратном порядке.
    // В аргумент $str может быть передана не только строка

    $strrev = "";
    $i = mb_strlen($str, "UTF-8");

    for ($i; $i >= 0; $i = $i - 1) {

        $strrev = $strrev . mb_substr($str, $i, 1, "UTF-8");

    }

    return $strrev;
}
