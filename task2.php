<?php

function createFileWithSum(string $pathToFiles): void
{
    // Даны два текстовых файла 1.txt и 2.txt. Они находятся в директории $pathToFiles
    // Каждый файл содержит по n целых чисел, располагающихся на отдельных строках.
    // Необходимо вычислить суммы чисел из двух файлов на соответствующих строках и записать их в файл 3.txt.
    // Файл 3.txt необходимо создать в директории $pathToFiles

    try {
        $f1 = fopen($pathToFiles.DIRECTORY_SEPARATOR."1.txt", "r");
        $f2 = fopen($pathToFiles.DIRECTORY_SEPARATOR."2.txt", "r");
        $f3 = fopen($pathToFiles.DIRECTORY_SEPARATOR."3.txt", "w");

        $sum = "";

        $one = false;

        if ($f1) {

            while (!feof($f1)) {
                if ($one) {

                    $sum = $sum . "\n";

                }

                $sloz = (int)fgets($f1) + (int)fgets($f2);

                $sum = $sum . (string)$sloz;

                $one = true;
            }

        }

        fwrite($f3, $sum);
        fclose($f1);
        fclose($f2);
        fclose($f3);
    }
    catch (Exception $e) {

    }
}
